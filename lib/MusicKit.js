'use strict';
/* jshint node: true */

/**
 * @typedef MusicKit
 * @type Object
 * @property {String} name I18N name of the music kit
 * @property {String} techName Technical name of the music kit
 * @property {String} defIndex Index/Key of the music kit
 */

/**
 * Standard return for a MusicKit.
 * @class
 */

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MusicKit =
/** @constructor */
function MusicKit(name, techName, defIndex) {
  _classCallCheck(this, MusicKit);

  this.name = name;
  this.techName = techName;
  this.defIndex = defIndex;
};

module.exports = MusicKit;