'use strict';
/* jshint node: true */

/**
 * @typedef Rarity
 * @type Object
 * @property {String} techName Technical name of the rarity
 * @property {String} weaponName I18N name applied to a weapon
 * @property {String} miscName I18N name applied to others objects (Medal, Music, ...)
 * @property {String} defIndex defIndex Index/Key of the Perfab Object
 * @property {String} color Hexadecimal color value that represent the quality
 */

/**
 * Standard return for a skin/paint
 * @class Weapon
 */

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Rarity =
/**
 * @constructor
 */
function Rarity(techName, weaponName, miscName, defIndex, color) {
  _classCallCheck(this, Rarity);

  this.techName = techName;
  this.weaponName = weaponName;
  this.miscName = miscName;
  this.defIndex = defIndex;
  this.color = color;
};

module.exports = Rarity;