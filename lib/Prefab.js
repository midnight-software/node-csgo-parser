'use strict';
/* jshint node: true */

/**
 * @typedef Prefab
 * @type Object
 * @property {String} name I18N name of the prefab object
 * @property {String} techName Technical name of the prefab object
 * @property {String} defIndex Index/Key of the prefab 
 * @property {String} rarity Rarity of this prefab object
 * @property {String} type Type of the prefab object
 */

/**
 * Standard return for a Prefab.
 * @class
 */

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Prefab =
/** @constructor */
function Prefab(name, techName, defIndex, rarity, type) {
  _classCallCheck(this, Prefab);

  this.name = name;
  this.techName = techName;
  this.defIndex = defIndex;
  this.rarity = rarity;
  this.type = type;
};

module.exports = Prefab;