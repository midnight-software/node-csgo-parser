'use strict';
/* jshint node: true */

/**
* @typedef Collection
* @type Object
* @property {String} name I18N name of the collection
* @property {String} techName Technical name of the collection
* @property {Array.<SkinPaint>} content skins List of skins for this Collection (Doe'nt contain knifes)
*/

/**
 * Standard return for a collection
 * @class Weapon
 */

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Collection =
/**
 * @constructor
 */
function Collection(name, techName, content) {
  _classCallCheck(this, Collection);

  this.name = name;
  this.techName = techName;
  this.content = content;
};

module.exports = Collection;