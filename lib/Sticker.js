'use strict';
/* jshint node: true */

/**
 * @typedef Sticker
 * @type Object
 * @property {String} name I18N name of the sticker
 * @property {String} techName Technical name of the sticker
 * @property {String} defIndex Index/Key of the sticker
 * @property {String} rarity Rarity of this sticker
 */

/**
 * Standard return for a Sticker.
 * @class
 */

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Sticker =
/** @constructor */
function Sticker(name, techName, defIndex, rarity) {
  _classCallCheck(this, Sticker);

  this.name = name;
  this.techName = techName;
  this.defIndex = defIndex;
  this.rarity = rarity;
};

module.exports = Sticker;